# Name:ROBOT04
# Desc:This robot copies all the images to the specified directory and renames the images
# updates the size of APK in the cloud
# after updating it sets the robot04_status flag to True
import os, zipfile
import fnmatch
import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb
import logging
import shutil
def remove(path):
    """ param <path> could either be relative or absolute. """
    if os.path.isfile(path):
        os.remove(path)  # remove the file
    elif os.path.isdir(path):
        shutil.rmtree(path)  # remove dir and all contains
    else:
        raise ValueError("file {} is not a file or dir.".format(path))

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, filename="ROBOT04_RUN_LOG.txt", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.info("----------ROBOT04 Starting...")


# ***********************************************MAIN LINE PROCESSING*************************************************
DIR_APK_FILES = "D:/Andomeda Robot/apk-downloader"
DIR_APK_UNZIP = "D:/Andomeda Robot/apkdownloader/APK_EXTRACTED"
DIR_IMAGE_LIB = "D:/Andomeda Robot/IMAGE_LIB"
file_count = 0
for root, dir, files in os.walk(DIR_APK_UNZIP):
        for old_img in fnmatch.filter(files, "*.jpg"):
            file_count += 1
            package_dir = root.replace(DIR_APK_UNZIP,"").split("\\")[1]
            new_img =  package_dir+"_"+old_img
            print new_img
            src_path = root+"\\"+old_img
            dst_path = DIR_IMAGE_LIB+"\\"+new_img
            shutil.copy2(src_path,dst_path)
            logging.info("copied:" + new_img)
print("total images copied:", file_count)

