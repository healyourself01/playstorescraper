# /*--------------------------ROBOT06---------------------------*/
#  ROBOT06
# Desc: It processes the image and applies a series of transformations
#       and saves the image with a prefix of "P" -Processed
# Setup: PIP INSTALL PILLOW
# /*--------------------------ROBOT06---------------------------*/

#Install pillow by using pip install pillow
import os
import fnmatch
from PIL import Image,ImageOps
from PIL import ImageFilter
from PIL import ImageEnhance
import glob

file_count = 0
DIR_IMAGE_LIB = "D:/Andomeda Robot/IMAGE_LIB"
DIR_IMAGE_PROCESSED = "D:\Andomeda Robot\IMAGE_LIB\processed_images"

for root, dir, files in os.walk(DIR_IMAGE_LIB):
        for item in fnmatch.filter(files, "*.jpg"):
            file_count += 1
            image = Image.open(root+"\\"+item)
            new = image.filter(ImageFilter.SHARPEN)
            new = new.filter(ImageFilter.DETAIL)
            new = new.filter(ImageFilter.SMOOTH)
            new = ImageEnhance.Color(new)
            factor = 1.2
            new = new.enhance(factor)
            new = ImageEnhance.Brightness(new)
            new = new.enhance(1.1)
            new = ImageEnhance.Sharpness(new)
            new = new.enhance(1.2)
            w, h = new.size
            new = new.crop((0, 0, w - 1, h - 1))
            new = ImageOps.autocontrast(new)
            #we can further improve by detecting the background colour and giving border accordingly
            new = ImageOps.expand(new, border=(2, 2), fill='black')
            to_image_path =  DIR_IMAGE_PROCESSED+"\\"+"p_"+item
            image.save(to_image_path)


