import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb

# The kind for the new entity
kind_name = 'playstoredata03'
# Instantiates a client
datastore_client = datastore.Client()


# query example

for entity in datastore_client.query(kind=kind_name).fetch():
    entity_key = datastore_client.key(kind_name,str(entity.key.name))
    datastore_client.delete(entity_key)

# query = datastore_client.query(kind = kind_name)
# for item in query.fetch():
#     print item['package']
