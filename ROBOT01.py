import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb

# Output directory
OUTPUT_DIR = "C:/Users/mindgame/Google Drive/APK_Downloads"
credentials = GoogleCredentials.get_application_default()
# Instantiates a client
datastore_client = datastore.Client()
# The kind for the new entity
kind_name = 'playstoredata04'
BASE_url = 'https://play.google.com/store/search?q=blouse%20designs&c=apps'

testdata = ['https://play.google.com/store/apps/details?id=com.HairColourIdeas.elfarras','https://play.google.com/store/apps/details?id=com.HairColorIdeas.qaizal']

"""
----------------------------------------------------------------------------------------------
function: add_entity
desc: checks whether the entity already exists, if not adds, else prints a message
input: key, entity
return:
----------------------------------------------------------------------------------------------
"""


def add_entity():
    datastore_client.put(app)
    return

    # query = datastore_client.query(kind=kind_name)
    # query.add_filter("package",app['package'])
    #
    # if not query.fetch():
    #     datastore_client.put(app)
    #     print app['package'], " added in the datastore successfully."
    # else:
    #     print app['package'], " already in the datastore."
# ----------------------------------------------------------------------------------------------


profile = webdriver.FirefoxProfile('C:/Users/ajay/AppData/Roaming/Mozilla/Firefox/Profiles/q1hsjq28.default')
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
# wait = WebDriverWait(driver, 10)


# test data setup
# for ele in testdata:
# # The Cloud Datastore key for the new entity
#     package = str(ele).split("=")[1]
#     app_key = datastore_client.key(kind_name, package)
#     # Prepares the new entity
#     app = datastore.Entity(key=app_key)
#     app['app_url'] = ele.decode('utf-8')
#     app['package'] = package.decode('utf-8')
#     if datastore_client.get(app_key):
#         print app['package'], " already in datastore.skipping.."
#     else:
#         datastore_client.put(app)
#         print app['package'], " added in the datastore successfully."

# The kind for the keywords
kind_name_keywords = 'keywords'
# kind for the startlinks
kind_startlinks = "startlinks"

# ---------------------LOOP00---------------------------------
# Loop through al the list of urls from starturls kind

query = datastore_client.query(kind=kind_startlinks)
query.add_filter('robot01_status', '=', False)
for link in query.fetch():
    wait = WebDriverWait(driver, 3)
    print link['url']
    driver.get(str(link['url']))
    # -------------LOOP01-------------------------------
    # Scroll to the bottom of the page for 10 times
    # Then only all the apps will come up in the page
    scroll_count = 0
    while scroll_count <= 5:
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(2)
        scroll_count += 1
    # -------------LOOP01-------------------------------
    # xpath - //a[@class='title'] - select all the anchor tags with class = title
    android_apps_found = driver.find_elements_by_xpath("//a[@class='title']")
    print "Total Apps found:", len(android_apps_found)
    #--------------------------- LOOP02 -----------------------------------------
    # Main Loop to extract all the title elements
    for ele in android_apps_found:
        # extract the package name by splitting the URL with = and convert it to unicode
        # sample URL - 'https://play.google.com/store/apps/details?id=com.HairColourIdeas.elfarras'
        # AFTER SPLIT - 'https://play.google.com/store/apps/details?id' 'com.HairColourIdeas.elfarras'
        package = str(ele.get_attribute('href')).split("=")[1].decode('utf-8')
        # Set the package name as key in datastore - it will be primary key
        app_key = datastore_client.key(kind_name, package)
        # Prepares the new entity
        app = datastore.Entity(key=app_key)
        # extract the app URL and package name
        app['app_url'] = ele.get_attribute("href")
        app['package'] = package
        app['robot01_date'] = time.asctime(time.localtime(time.time()))
        app['robot01_status'] = True
        app['robot02_status'] = False
        app['robot03_status'] = False
        # if the package name already exists, skip else write the new entity
        # fetch the record using the package name as key using GET method
        if datastore_client.get(app_key):
            print app['package'], " already in datastore.skipping.."
        else:
            datastore_client.put(app)
            print app['package'], " added in the datastore successfully."
    # --------------------------- LOOP02 -----------------------------------------
    # update the link that it has been crawled
    link['robot01_status'] = True
    datastore_client.put(link)

# close the browser
driver.quit()