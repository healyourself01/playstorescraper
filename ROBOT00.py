import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb


#
# it reads the keywords and developers list from the kewords kind and
# builds the list of URLs to crawl the playstore
#
#

# query string base url
query_base_head = 'https://play.google.com/store/search?q='
query_base_tail = '&c=apps'
# developer string base url
developer_base = 'https://play.google.com/store/apps/developer?id='

# The kind for the keywords
kind_name_keywords = 'keywords'
# kind for the startlinks
kind_startlinks = "startlinks"

# Instantiates a client
datastore_client = datastore.Client()

query = datastore_client.query(kind=kind_name_keywords)

for item in query.fetch():
    terms = item['terms']
    developers = item['developers']

# print terms
# print developers
#
# # first delete all the entities in kind startlinks
# for url in datastore_client.query(kind=kind_startlinks).fetch():
#     # link_key = datastore_client.key(kind_startlinks,str(url.key.id).decode('utf-8'))
#     # link = datastore.Entity(key=link_key)
#     datastore_client.delete(url)


for term in terms:
    print query_base_head+term+query_base_tail
    # Set the package name as key in datastore - it will be primary key
    link_key = datastore_client.key(kind_startlinks,str(term).decode('utf-8'))
    # Prepares the new entity
    link = datastore.Entity(key=link_key)
    # extract the app URL and package name
    link['url'] = str(query_base_head+term+query_base_tail).decode('utf-8')
    link['keyword'] = str(term).decode('utf-8')
    link['robot00_date'] = time.asctime(time.localtime(time.time()))
    link['robot01_status'] = False
    datastore_client.put(link)


for developer in developers:
    print developer_base+developer
    # Set the package name as key in datastore - it will be primary key
    link_key = datastore_client.key(kind_startlinks,str(developer).decode('utf-8'))
    # Prepares the new entity
    link = datastore.Entity(key=link_key)
    # extract the app URL and package name
    link['url'] = str(developer_base+developer).decode('utf-8')
    link['keyword'] = str(developer).decode('utf-8')
    link['robot01_status'] = False
    datastore_client.put(link)
