# Name:ROBOT03
# Desc:This robot downloads the APK fiels and then saves thenm in the google drive.
# puts the apk path in the cloud
# after updating it sets the robot03_status flag to True

import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb
import logging

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, filename="ROBOT03_RUN_LOG.txt", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.info("----------ROBOT03 Starting...")

# Output directory
OUTPUT_DIR = "C:/Users1/ajay/Google Drive/development"
credentials = GoogleCredentials.get_application_default()
# Instantiates a client
datastore_client = datastore.Client()
testdata = ['https://play.google.com/store/apps/details?id=com.HairColourIdeas.elfarras','https://play.google.com/store/apps/details?id=com.HairColorIdeas.qaizal']


# profile path for firefox
profile = webdriver.FirefoxProfile('C:/Users/ajay/AppData/Roaming/Mozilla/Firefox/Profiles/q1hsjq28.default')
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))

# profile.add_extension(folder_xpi_file_saved_in + "\/quickjava-2.0.6-fx.xpi")
profile.set_preference("thatoneguydotnet.QuickJava.curVersion","2.0.6.1")  ## Prevents loading the 'thank you for installing screen'
profile.set_preference("thatoneguydotnet.QuickJava.startupStatus.Images", 2)  ## Turns images off
profile.set_preference("thatoneguydotnet.QuickJava.startupStatus.AnimatedImage", 2)  ## Turns animated images off
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)

from selenium.common.exceptions import NoSuchElementException


def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


# The kind for the keywords
kind_name_keywords = 'keywords'
# kind for the startlinks
kind_startlinks = "startlinks"
# The kind for the new entity
kind_playstoredata = 'playstoredata04'


query = datastore_client.query(kind =kind_playstoredata)
query.add_filter('robot03_status', '=', False)

# -------------LOOP01-------------------------------
android_apps_found = query.fetch()
print "Total Apps found for processing ROBOT02:", len(list(android_apps_found))
#--------------------------- LOOP02 -----------------------------------------
# Main Loop to extract all the title elements
for app in query.fetch():
    driver.get(app['app_url'])
    wait._driver.find_element_by_xpath("/html/body/div[5]/div[6]/div/div/div[1]/div[1]/div/div[1]/div/div[3]/div/div[1]/span/span[2]").click();
    app['robot03_date'] = str(time.asctime(time.localtime(time.time()))).decode('utf-8')
    # Set the package name as key in datastore - it will be primary key
    app_key = datastore_client.key(kind_playstoredata, str(app.key.name))
    # extract the app URL and package name
    app['robot03_status'] = True
    app['download_status'] = True
    app['path'] = str("C:/Users/ajay/Downloads/apk-downloader").decode('utf-8')
    # if the package name already exists, skip else write the new entity
    # fetch the record using the package name as key using GET method
    # *****DONT SET THE KEY IF YOU WANT TO UPDATE*****
    datastore_client.put(app)
    logging.info("Downloaded App:%s", str(app['package']))
    print app['package'], " updated with download info in datastore."
    time.sleep(3) #sleep for 3 seconds
# close the browser
logging.info("----------ROBOT03 Ending...")
driver.quit()