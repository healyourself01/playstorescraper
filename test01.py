import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb

# The kind for the new entity
kind_name = 'startlinks'
# Instantiates a client
datastore_client = datastore.Client()


# query example

query = datastore_client.query(kind=kind_name)
query.add_filter('robot01_status', '=', False)
print query.fetch()
# update example
# for entity in datastore_client.query(kind='playstoredata03').fetch():
#     entity['creator'] = str("ajay").decode('utf-8')
#     datastore_client.put(entity)

# query = datastore_client.query(kind = kind_name)
# for item in query.fetch():
#     print item['package']
