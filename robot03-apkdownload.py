import os
import csv
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Output directory
OUTPUT_DIR = "C:/Users/mindgame/Google Drive/APK_Downloads"


profile = webdriver.FirefoxProfile('c:/Users/mindgame/AppData/Roaming/Mozilla/Firefox/Profiles/o5p4oabs.default-1485872485191')
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)
with open('google.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        driver.get(row[0])
        wait._driver.find_element_by_xpath("/html/body/div[5]/div[6]/div/div/div[1]/div[1]/div/div[1]/div/div[3]/div/div[1]/span/span[2]").click();
