# Name:ROBOT02
# Desc:This robot extracts the data from the app detail page and
# updates the playstore data kind in cloud store.
# after updating it sets the robot02_status flag to True
#
#

import os
import csv
import time
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from google.appengine.ext import ndb
import logging
# from xvfbwrapper import Xvfb

# display = Xvfb()
# Start the display
# display.start()

# now Firefox will run in a virtual display.
# you will not see the browser.

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, filename="ROBOT02_RUN_LOG.txt", filemode="w",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.info("----------ROBOT02 Starting...")

# Output directory
OUTPUT_DIR = "C:/Users/mindgame/Google Drive/APK_Downloads"
FIREFOX_PROFILE = "C:/Users/Ajay/AppData/Roaming/Mozilla/Firefox/Profiles/rh35ia2y.default"
credentials = GoogleCredentials.get_application_default()
print str(credentials)
# Instantiates a client
datastore_client = datastore.Client(project="vaulted-zodiac-160512")
testdata = ['https://play.google.com/store/apps/details?id=com.HairColourIdeas.elfarras','https://play.google.com/store/apps/details?id=com.HairColorIdeas.qaizal']


# profile path for firefox
profile = webdriver.FirefoxProfile(FIREFOX_PROFILE)
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))

# profile.add_extension(folder_xpi_file_saved_in + "//quickjava-2.0.6-fx.xpi")
profile.set_preference("thatoneguydotnet.QuickJava.curVersion","2.0.6.1")  ## Prevents loading the 'thank you for installing screen'
profile.set_preference("thatoneguydotnet.QuickJava.startupStatus.Images", 2)  ## Turns images off
profile.set_preference("thatoneguydotnet.QuickJava.startupStatus.AnimatedImage", 2)  ## Turns animated images off

from selenium.common.exceptions import NoSuchElementException


def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

driver = webdriver.Firefox(profile)
# The kind for the keywords
kind_name_keywords = 'keywords'
# kind for the startlinks
kind_startlinks = "startlinks"
# The kind for the new entity
kind_playstoredata = 'playstoredata04'


query = datastore_client.query(kind =kind_playstoredata)

# query.add_filter('robot02_status', '=', False)  #ENABLE -DISABLED FOR TESTING

# -------------LOOP01-------------------------------
android_apps_found = query.fetch()
print "Total Apps found for processing ROBOT02:", len(list(android_apps_found))
#--------------------------- LOOP02 -----------------------------------------
# Main Loop to extract all the title elements
for app in query.fetch():
    driver.get(app['app_url'])
    if check_exists_by_xpath("//div[@class='id-app-title']") is True:
        app['title'] = driver.find_element_by_xpath("//div[@class='id-app-title']").text
    else:
        app['title'] = None

    if check_exists_by_xpath("//div[@class='score']") is True:
        app['rating'] = float(driver.find_element_by_xpath("//div[@class='score']").text)
    else:
        app['rating'] = None

    app['developer'] = driver.find_element_by_xpath("//span[@itemprop='name']").text

    if check_exists_by_xpath("//div[@itemprop='numDownloads']") is True:
        app['installs'] = driver.find_element_by_xpath("//div[@itemprop='numDownloads']").text
    else:
        app['installs'] = None

    app['date'] = driver.find_element_by_xpath("//div[@itemprop='datePublished']").text
    app['robot02_date'] = str(time.asctime(time.localtime(time.time()))).decode('utf-8')
    # Set the package name as key in datastore - it will be primary key
    app_key = datastore_client.key(kind_playstoredata, str(app.key.name))
    # extract the app URL and package name
    app['robot02_status'] = True
    # if the package name already exists, skip else write the new entity
    # fetch the record using the package name as key using GET method
    # *****DONT SET THE KEY IF YOU WANT TO UPDATE*****
    datastore_client.put(app)
    logging.info("Install details added for:%s", str(app['package']))
    print app['package'], " updated with install details in datastore."
# close the browser
logging.info("----------ROBOT02 Ending...")
driver.quit()
# Stop the display
# display.stop()